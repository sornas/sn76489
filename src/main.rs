use rodio::OutputStream;
use sn76489::SN76489AN;

fn main() {
    let (_stream, stream_handle) = OutputStream::try_default().unwrap();
    let (sn, sn_io) = SN76489AN::new();
    sn_io.tone0_freq(284);
    sn_io.tone1_freq((284 * 3 / 2) as u16);
    sn_io.tone2_freq((284 / 2) as u16);
    sn_io.tone0_attenuation(0b1111);
    sn_io.tone1_attenuation(0b1111);
    sn_io.tone2_attenuation(0b1111);
    stream_handle.play_raw(sn).unwrap();

    loop {
        sn_io.tone0_attenuation(0b1100);
        sn_io.tone1_attenuation(0b1100);
        sn_io.tone2_attenuation(0b1100);
        std::thread::sleep(std::time::Duration::from_millis(30));
        sn_io.tone0_attenuation(0b1101);
        sn_io.tone1_attenuation(0b1101);
        sn_io.tone2_attenuation(0b1101);
        std::thread::sleep(std::time::Duration::from_millis(40));
        sn_io.tone0_attenuation(0b1110);
        sn_io.tone1_attenuation(0b1110);
        sn_io.tone2_attenuation(0b1110);
        std::thread::sleep(std::time::Duration::from_millis(60));
        sn_io.tone0_attenuation(0b1111);
        sn_io.tone1_attenuation(0b1111);
        sn_io.tone2_attenuation(0b1111);
        std::thread::sleep(std::time::Duration::from_millis(500));
    }
}
