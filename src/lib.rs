use parking_lot::Mutex;
use rclite::Arc;
use rodio::Source;

struct ToneGen {
    freq: u16,
    counter: u16,
    amplitude: f32,
    output: f32,
}

impl ToneGen {
    fn attenuation(&mut self, n: u8) {
        assert!(n <= 15);
        if n == 15 {
            self.amplitude = 0.0;
        } else {
            self.amplitude = 10.0_f32.powf(-(n as f32) / 10.0)
        }
    }
}

impl Default for ToneGen {
    fn default() -> Self {
        Self {
            freq: 0,
            counter: 0,
            amplitude: 0.0,
            output: 1.0,
        }
    }
}

impl Iterator for ToneGen {
    type Item = f32;

    fn next(&mut self) -> Option<Self::Item> {
        self.counter += 1;

        if self.counter >= self.freq {
            self.counter = 0;
            self.output *= -1.0;
        }

        Some(self.output * self.amplitude)
    }
}

struct NoiseGen {
    shift_rate: u16, // 16, 32, 64, tone 3 * 32
    amplitude: f32,
    output: f32,
    counter: u16,
}

impl NoiseGen {
    fn attenuation(&mut self, n: u8) {
        assert!(n <= 15);
        if n == 15 {
            self.amplitude = 0.0;
        } else {
            self.amplitude = 10.0_f32.powf(-(n as f32) / 10.0)
        }
    }
}

impl Default for NoiseGen {
    fn default() -> Self {
        Self {
            shift_rate: 16,
            amplitude: 0.0,
            output: 1.0,
            counter: 0,
        }
    }
}

impl Iterator for NoiseGen {
    type Item = f32;

    fn next(&mut self) -> Option<Self::Item> {
        self.counter += 1;

        if self.counter >= self.shift_rate {
            self.counter = 0;
            self.output = if sungod::Ra::ggen::<bool>() {
                self.amplitude
            } else {
                -self.amplitude
            };
        }

        Some(self.output)
    }
}

pub struct SN76489AN {
    tone0: Arc<Mutex<ToneGen>>,
    tone1: Arc<Mutex<ToneGen>>,
    tone2: Arc<Mutex<ToneGen>>,
    noise: Arc<Mutex<NoiseGen>>,
}

#[allow(non_camel_case_types)]
pub struct SN76489AN_IO {
    tone0: Arc<Mutex<ToneGen>>,
    tone1: Arc<Mutex<ToneGen>>,
    tone2: Arc<Mutex<ToneGen>>,
    noise: Arc<Mutex<NoiseGen>>,
}

impl SN76489AN {
    pub fn new() -> (Self, SN76489AN_IO) {
        let tone0 = Arc::new(Mutex::new(ToneGen::default()));
        let tone1 = Arc::new(Mutex::new(ToneGen::default()));
        let tone2 = Arc::new(Mutex::new(ToneGen::default()));
        let noise = Arc::new(Mutex::new(NoiseGen::default()));

        (
            Self {
                tone0: tone0.clone(),
                tone1: tone1.clone(),
                tone2: tone2.clone(),
                noise: noise.clone(),
            },
            SN76489AN_IO {
                tone0,
                tone1,
                tone2,
                noise,
            },
        )
    }
}

impl SN76489AN_IO {
    pub fn tone0_freq(&self, n: u16) {
        self.tone0.lock().freq = n;
    }
    pub fn tone1_freq(&self, n: u16) {
        self.tone1.lock().freq = n;
    }
    pub fn tone2_freq(&self, n: u16) {
        self.tone2.lock().freq = n;
    }
    pub fn noise_rate(&self, nfo: bool, nfi: bool) {
        match (nfo, nfi) {
            (false, false) => {
                self.noise.lock().shift_rate = 16;
            }
            (false, true) => {
                self.noise.lock().shift_rate = 32;
            }
            (true, false) => {
                self.noise.lock().shift_rate = 64;
            }
            (true, true) => todo!(),
        }
    }

    pub fn tone0_attenuation(&self, n: u8) {
        self.tone0.lock().attenuation(n);
    }
    pub fn tone1_attenuation(&self, n: u8) {
        self.tone1.lock().attenuation(n);
    }
    pub fn tone2_attenuation(&self, n: u8) {
        self.tone2.lock().attenuation(n);
    }
    pub fn noise_attenuation(&self, n: u8) {
        self.noise.lock().attenuation(n);
    }
}

impl Iterator for SN76489AN {
    type Item = f32;

    fn next(&mut self) -> Option<Self::Item> {
        Some(
            self.tone0.lock().next().unwrap()
                + self.tone1.lock().next().unwrap()
                + self.tone2.lock().next().unwrap()
                + self.noise.lock().next().unwrap(),
        )
    }
}

impl Source for SN76489AN {
    fn current_frame_len(&self) -> Option<usize> {
        None
    }

    fn channels(&self) -> u16 {
        1
    }

    fn sample_rate(&self) -> u32 {
        125_000
    }

    fn total_duration(&self) -> Option<std::time::Duration> {
        None
    }
}
